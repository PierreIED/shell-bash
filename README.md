# Shell bash

## Overview

Shell bash simplifié fonctionnant en ligne de commande. Il est implémenté en c et testé dans un environnement linux (Ubuntu). Les fonctionnalités implémentées sont les suivantes : 

- Redirections : possibilité de changer le flux d'entrée et de sortie des commandes
- pipes : possibilité d'utiliser le résultat d'une commande dans une deuxième commande
- Fonctions : 
    - kill : identique à la fonction kill d'un shell bash (utilisation des signaux)
    - sleep : pause dans l'exécution de la commande
    - man : accès aux pages de manuel correspondant aux présentes fonctions
    - cp : copie d'un fichier
    - cd : changement de répertoire (fonction interne)
    - quit : provoque la fermeture du programme

## Contexte

Ce projet a été réalisé dans le cadre du cours de Licence 2ème année de M. Philippe Kislin : Systèmes d'exploitations. Le but de ce projet a été de comprendre le fonctionnement interne d'un shell bash afin d'en maîtriser un maximum d'aspects. 

## Installation

L'installation se fait par le makefile : après avoir cloné le repo, taper la commande "make" dans le répertoire contenant le makefile. 

Un fichier nommé "monShell" est alors créé. Il suffit de le rendre exécutable (sous linux : sudo chmod +x monShell), puis de le lancer. 

## Usage

Une fois installé, le programme fonctionne comme un shell bash classique (toutefois limité par le nombre réduit de commandes implémentées).

## Visuals
 coming soon



## Remerciements

Remerciements tout particuliers à M. Philippe Kislin pour son implication envers ses étudiants et les progrès qu'il m'a permis de réaliser. 

## License
licence GNU-GPL
